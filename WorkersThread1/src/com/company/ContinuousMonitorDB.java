package com.company;

import java.util.ArrayList;

public class ContinuousMonitorDB implements Runnable {

    private boolean bShouldContinue = true;
    private ArrayList<String> arrayList = null;
    private DbtoConsole dbc = new DbtoConsole();

    public ContinuousMonitorDB(){
        arrayList = dbc.Sqltojava();
    }

    @Override
    public void run () {

        while(bShouldContinue){
            ArrayList<String> newList = dbc.Sqltojava();
            for (String fileName: newList)
            {
                if(!arrayList.contains(fileName))
                {
                    print(fileName);
                    arrayList.add(fileName);
                }
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void print(String details){

    }
    }






