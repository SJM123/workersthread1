package com.company;

import java.io.*;
import java.sql.SQLException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileMonitorContinuous implements Runnable {

    private boolean aShouldContinue = true;

    @Override
    public void run() {

            String directoryName = "/home/training/IdeaProjects/WorkersThread/src/com/company/";

            while (aShouldContinue) {
                File directory = new File(directoryName);

                File[] fList = directory.listFiles();
                BufferedReader reader;
                ConsoleTODb ctd = new ConsoleTODb();
                try {

                    for (File file : fList) {
                        reader = new BufferedReader(new FileReader(file));
                        String allLines = "";
                        String line;
                        while ((line = reader.readLine()) != null) {
                            allLines += line;
                        }
                        try {

                            if (file.getName().contains((".txt"))) {
                                System.out.println(file.getName());
                                System.out.println(file.length() + "  bytes");


                                ctd.insert(file.getName(), file.length() + "  bytes", allLines);
                            }


                        } catch (SQLException e) {

                        } catch (ClassNotFoundException e) {

                        }
                    }
                } catch (FileNotFoundException t) {

                } catch (IOException e) {
                }
            }
    }
}

